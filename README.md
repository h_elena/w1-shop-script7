=== Wallet One Payment ===
Contributors: Wallet One
Version: 1.0.0
Tags: Wallet One, Shop-script7, buy now, payment, payment for Shop-script7
Requires at least: 7.1.2.63
Tested up to: 7.2.0.102
Stable tag: 7.2.0.102
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One module is a payment system for Shop-script7. He it allows to pay for your orders on the site.

== Description ==
If you have an online store on Readyscript, then you need a module payments for orders made. This will help you plug the payment system Wallet One. With our system you will be able to significantly expand the method of receiving payment. This will lead to an increase in the number of customers to your store.

The latest version of the module you can be found at the link https://bitbucket.org/h_elena/w1-shop-script7/downloads

== Installation ==
1. Register on the site http://www.walletone.com
2. Download the module files on this and upload to the server in a folder /modules/.
3. Activate the module.
4. Instructions for configuring the module is in the file read.pdf or go to site https://www.walletone.com/ru/merchant/modules/readyscript/ and read instuction.

== Screenshots ==

== Changelog ==
= 1.0.0 =
*- Adding a module

== Frequently Asked Questions ==
No recent asked questions 

== Upgrade Notice ==
No recent asked updates

