<?php

/**
 *
 * @author Wallet One
 * @name Wallet One
 * @description Wallet One Payment Gateway
 * @link https://www.walletone.com
 *
 * Plugin settings parameters must be specified in file lib/config/settings.php
 * @property-read string $email Merchant email
 * @property-read bool $sandbox Sandbox mode flag
 * @property-read bool[] $currency Array with currency codes as keys and their settings values
 */

class w1Payment extends waPayment implements waIPayment {
  /**
   * @var string
   */
  private $order_id;

  protected function initControls() {
    include_once dirname(__DIR__).'/walletone/Classes/W1Client.php';
    
    $this
      ->registerControl('OrderStatusSelected')
      ->registerControl('PaymentsMethod');
    
    parent::initControls();
  }
  
  /**
   * Created HTML code for 'PaymentsMetod' in admin panel.
   *
   * @see waHtmlControl::getControl()
   * @param string $name Идентификатор элемента управления
   * @param array $params Параметры элемента управления
   * @return string
   */
  public static function settingOrderStatusSelected($name, $params = array()) {
    $cfg = shopWorkflow::getConfig();
    $html = '<select name="'.$name.'">';
    foreach (ifset($cfg['states'], array()) as $id => $state) {
      $html .= '<option value="'.$id.'"'.(!empty($params['value']) && $params['value'] == $id ? ' selected' : '').'>'.$state['name'].'</option>';
    }
    $html .= '</select>';
    return $html;
  }
  
  /**
   * Created HTML code for 'PaymentsMetod' in admin panel.
   *
   * @see waHtmlControl::getControl()
   * @param string $name Идентификатор элемента управления
   * @param array $params Параметры элемента управления
   * @return string
   */
  public static function settingPaymentsMethod($name, $params = array()) {
    $names = 'ptenabled';
    if(strpos($name, 'ptdisabled') !== false){
      $names = 'ptdisabled';
    }
    $client = \WalletOne\Classes\W1Client::init()->run('ru', 'shopscript');
    $html = $client->getHtml()->getPayments($names, $params['value'], $client->getHtml()->filename);
    $html .= '<link href="'.str_replace($_SERVER['DOCUMENT_ROOT'], '', str_replace('\\', '/', __DIR__)).'/../walletone/css/shopscript7.css" type="text/css" rel="stylesheet">';
    return $html;
  }
  
  public function getGuide($params = array()){
    parent::getGuide($params);
    
    include_once dirname(__DIR__).'/walletone/Classes/W1Client.php';
    $client = \WalletOne\Classes\W1Client::init()->run('ru', 'shopscript');
    $logger = \Logger::getLogger(__CLASS__);
    $adapter = $this->getAdapter();
    $adapter->setSettings($this->id, $this->key, 'data_update', time());
  }

  /**
   * Current currency verification.
   */
  public function allowedCurrency(){
    $model = new shopPluginModel();
    $plugin = $model->getPlugin($this->merchant_id, 'payment');
    include_once dirname(__DIR__).'/walletone/Classes/W1Client.php';
    $client = \WalletOne\Classes\W1Client::init()->run('ru', 'shopscript');
    $logger = \Logger::getLogger(__CLASS__);
    if(!empty($this->generate) && $this->generate == 'yes'){
      if(!file_exists(dirname(__DIR__).'/walletone/img/new_icon.png') || filemtime(dirname(__DIR__).'/walletone/img/new_icon.png') < $this->data_update){
        if($img_src = $client->createNewIcon($this->ptenabled, $this->ptdisabled)) {
          if (strpos($plugin['description'], '<img') !== false) {
            if (preg_match('/<img([^&]*)">/', $plugin['description'], $matches)) {
              if (preg_match('/"([^&]*)/', $matches[1], $matches)) {
                $plugin['description'] = str_replace($matches[1], '/wa-plugins/payment/w1/walletone/img/'.$img_src, $plugin['description']);
              }
            }
          }
          else {
            $plugin['description'] .= ' <img src="' . '/wa-plugins/payment/w1/walletone/img/'. $img_src . '">';
          }
          $shipping = $model->listPlugins(shopPluginModel::TYPE_SHIPPING, array('payment' => $this->merchant_id, 'all' => true));
          try {
            $plugin = shopPayment::savePlugin($plugin);
            $app_settings = new waAppSettingsModel();
            $mas_shipping_enabled = array();
            $mas_shipping_disabled = array();
            $settings[$this->merchant_id] = array();
            foreach($shipping as $ship){
              if(!empty($ship['available']) && $ship['available'] == 1) {
                $mas_shipping_enabled[$this->merchant_id][] = $ship['id'];
              }
              else{
                $mas_shipping_disabled[$this->merchant_id][] = $ship['id'];
              }
            }
            $app_settings->set('shop', 'shipping_payment_disabled', json_encode($mas_shipping_disabled));
            $app_settings->set('shop', 'shipping_payment_enabled', json_encode($mas_shipping_enabled));
          }
          catch (Exception $exc) {
            $this->logger->info($exc->getTraceAsString());
          }
        }
      }
    }
    elseif (strpos($plugin['description'], '<img') !== false) {
      if (preg_match('/<img([^&]*)>/', $plugin['description'], $matches) !== false) {
        $plugin['description'] = str_replace($matches[0], '', $plugin['description']);
        $shipping = $model->listPlugins(shopPluginModel::TYPE_SHIPPING, array('payment' => $this->merchant_id, 'all' => true));
          try {
            $plugin = shopPayment::savePlugin($plugin);
            $app_settings = new waAppSettingsModel();
            $mas_shipping_enabled = array();
            $mas_shipping_disabled = array();
            $settings[$this->merchant_id] = array();
            foreach($shipping as $ship){
              if(!empty($ship['available']) && $ship['available'] == 1) {
                $mas_shipping_enabled[$this->merchant_id][] = $ship['id'];
              }
              else{
                $mas_shipping_disabled[$this->merchant_id][] = $ship['id'];
              }
            }
            $app_settings->set('shop', 'shipping_payment_disabled', json_encode($mas_shipping_disabled));
            $app_settings->set('shop', 'shipping_payment_enabled', json_encode($mas_shipping_enabled));
          }
          catch (Exception $exc) {
            $this->logger->info($exc->getTraceAsString());
          }
      }
    }
    
    return ($this->currencyId && array_key_exists($this->currencyId, $client->getSettings()->currencyCode) ? $client->getSettings()->currencyCode[$this->currencyId] : 'RUB');
  }

  /**
   * Returns array of transaction operations supported by payment gateway.
   *
   * See available list of operation types as OPERATION_*** constants of waPayment.
   * @return array
   */
  public function supportedOperations() {
    return array(
      self::OPERATION_AUTH_CAPTURE,
    );
  }

  /**
   * Generates payment form HTML code.
   *
   * Payment form can be displayed during checkout or on order-viewing page.
   * Form "action" URL can be that of the payment gateway or of the current page (empty URL).
   * In the latter case, submitted data are passed again to this method for processing, if needed;
   * e.g., verification, saving, forwarding to payment gateway, etc.
   * @param array $payment_form_data Array of POST request data received from payment form
   * (if no "action" URL is specified for the form)
   * @param waOrder $order_data Object containing all available order-related information
   * @param bool $auto_submit Whether payment form data must be automatically submitted (useful during checkout)
   * @return string Payment form HTML
   * @throws waException
   */
  public function payment($payment_form_data, $order_data, $auto_submit = false) {
    // using order wrapper class to ensure use of correct data object
    $order = waOrder::factory($order_data);
    
    include_once dirname(__DIR__).'/walletone/Classes/W1Client.php';
    $client = \WalletOne\Classes\W1Client::init()->run('ru', 'shopscript');
    $logger = \Logger::getLogger(__CLASS__);
    //verifying order currency with supported currency
    if (!in_array($order->currency, $client->getSettings()->currencyCode)) {
      throw new waException(w1ErrorCurrency);
    }
    
    $successUrl = $this->getAdapter()->getBackUrl(waAppPayment::URL_SUCCESS, $order);
    $failUrl = $this->getAdapter()->getBackUrl(waAppPayment::URL_DECLINE);
    $settings = [
      'merchantId' => $this->merchantId,
      'signatureMethod' => $this->signatureMethod,
      'signature' => $this->signature,
      'currencyId' => $this->currencyId,
      'currencyDefault' => 0,
      'orderStatusSuccess' => '',
      'orderStatusWaiting' => '',
      'cultureId' => 'ru-RU',
      'order_currency' => array_search($order->currency, $client->getSettings()->currencyCode),
      'summ' => number_format($order->total, 2, '.', ''),
      'orderId' =>  $order->id,
      'siteName' => wa()->getSetting('name', '', 'shop'),
      'nameCms' => '_shopScript7',
      'successUrl' => $successUrl,
      'failUrl' => $failUrl,
      'app_id' => $this->app_id,
      'merchant_id' => $this->merchant_id
    ];
    
    if(!empty($this->ptenabled)){
      $settings['paymentSystemEnabled'] = $this->ptenabled;
    }
    if(!empty($this->ptdisabled)){
      $settings['paymentSystemDisabled'] = $this->ptdisabled;
    }
    if(!empty($this->email)){
      $settings['email'] = $this->email;
    }

    if ($client->validateParams($settings) !== true) {
      $logger->info($validate['error']);
      echo $validate['error'];
      return;
    }
    $fields = $client->createFieldsForForm();
    
    $view = wa()->getView();
    $view->assign(
        array(
          'url' => wa()->getRootUrl(),
          'hidden_fields' => $fields,
          'form_url' => $client->getPayments()->paymentUrl,
          'auto_submit' => $auto_submit,
          'plugin' => $this,
          'text_submit' => w1OrderSubmitShort
        )
    );
    
    //connection template with payment form
    return $view->fetch($this->path . '/templates/payment.html');
  }
  

  /**
   * Plugin initialization for processing callbacks received from payment gateway.
   *
   * To process callback URLs of the form /payments.php/w1/*,
   * corresponding app and id must be determined for correct initialization of plugin settings.
   * @param array $request Request data array ($_REQUEST)
   * @return waPayment
   * @throws waPaymentException
   */
  protected function callbackInit($request) {
    // parsing data to obtain order id as well as ids of corresponding app and plugin setup instance responsible
    // for callback processing
    if (isset($request['WMI_PAYMENT_NO']) && is_numeric(str_replace('_'.$_SERVER['HTTP_HOST'], '', $request['WMI_PAYMENT_NO']))) {
      $this->order_id = str_replace('_'.$_SERVER['HTTP_HOST'], '', $request['WMI_PAYMENT_NO']);
      $this->app_id = ifempty($request['app_id']);
      $this->merchant_id = ifempty($request['merchant_id']);
    }
    else {
      include_once dirname(__DIR__).'/walletone/Classes/W1Client.php';
      $client = \WalletOne\Classes\W1Client::init()->run('ru', 'shopscript');
      $logger = \Logger::getLogger(__CLASS__);
      $logger->info('Invalid invoice number');
      throw new waPaymentException('Invalid invoice number');
    }
    return parent::callbackInit($request);
  }

  /**
   * Actual processing of callbacks from payment gateway.
   *
   * Request parameters are checked and app's callback handler is called, if necessary.
   * Plugin settings are already initialized and available.
   * IPN (Instant Payment Notification)
   * @throws waPaymentException
   * @param array $request Request data array ($_REQUEST) received from gateway
   * @return array Associative array of optional callback processing result parameters:
   *     'redirect' => URL to redirect user upon callback processing
   *     'template' => path to template to be used for generation of HTML page displaying callback processing results;
   *                   false if direct output is used
   *                   if not specified, default template displaying message 'OK' is used
   *     'header'   => associative array of HTTP headers ('header name' => 'header value') to be sent to user's
   *                   browser upon callback processing, useful for cases when charset and/or content type are
   *                   different from UTF-8 and text/html
   *
   *     If a template is used, returned result is accessible in template source code via $result variable,
   *     and method's parameters via $params variable
   */
  protected function callbackHandler($request) {
    include_once dirname(__DIR__).'/walletone/Classes/W1Client.php';
    $client = \WalletOne\Classes\W1Client::init()->run('ru', 'shopscript');
    $logger = \Logger::getLogger(__CLASS__);
    // verifying that order id was received within callback
    if (!$this->order_id) {
      ob_start();
      $logger->info(w1ErrorEmptyOrderId);
      echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.w1ErrorEmptyOrderId;
      die();
    }
    
    $settings = [
      'merchantId' => $this->merchantId,
      'signature' => $this->signature,
      'signatureMethod' => $this->signatureMethod,
      'currencyId' => $this->currencyId,
      'currencyDefault' => 0,
      'orderStatusSuccess' => '',
      'orderStatusWaiting' => ''
    ];
    
    $settings['orderPaymentId'] = $_POST['WMI_ORDER_ID'];
    $settings['orderState'] = mb_strtolower($_POST['WMI_ORDER_STATE']);
    $settings['orderId'] = str_replace('_' . $_SERVER['HTTP_HOST'], '', $_POST['WMI_PAYMENT_NO']);
    $settings['paymentType'] = $_POST['WMI_PAYMENT_TYPE'];
    $settings['summ'] = $_POST['WMI_PAYMENT_AMOUNT'];
    $logger->info($settings);
    if ($client->resultValidation($settings, $_POST)) {
      $result = $client->getResult();
      
      $order_model = new shopOrderModel();
      if (!$order = $order_model->getById($this->order_id)) {
        $error = sprintf(w1ErrorResultOrder, $result->orderId, $result->orderState, $result->orderPaymentId);
        $logger->info($error);
        ob_start();
        echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.$error;
        die();
      }
      
      //checking on the order amount
      if (number_format($order['total'], 2, '.', '') != $result->summ) {
        $error = sprintf(w1ErrorResultOrderSumm, $result->orderId, $result->orderState, $result->orderPaymentId);
        $logger->info($error);
        ob_start();
        echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.$error;
        die();
      }
      
      if($result->orderState == 'accepted'){
        $transaction_data = $this->formalizeData($request);
        $transaction_data['order_id'] = $this->order_id;
        $transaction_data['plugin'] = $this->id;
        $callback_method = self::CALLBACK_PAYMENT;
        $transaction_data = $this->saveTransaction($transaction_data, $request);
        $this->execAppCallback($callback_method, $transaction_data);
        $text = sprintf(w1OrderResultSuccess, $result->orderId, $result->orderState, $result->orderPaymentId);
        $logger->info($text);
        ob_start();
        echo 'WMI_RESULT=OK';
        die();
      }
    }
    
    $logger->info('Error');
    ob_start();
    echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION=Error';
    die();
  }

  /**
   * Converts raw transaction data received from payment gateway to acceptable format.
   *
   * @param array $request Raw transaction data
   * @return array $transaction_data Formalized data
   */
  protected function formalizeData($request) {
    // obtaining basic request information
    $transaction_data = parent::formalizeData($request);
    $transaction_data['native_id'] = $this->order_id;
    $transaction_data['order_id'] = $this->order_id;
    $transaction_data['amount'] = ifempty($request['WMI_PAYMENT_AMOUNT'], '');
    $transaction_data['currency_id'] = $this->merchant_currency;
    
    return $transaction_data;
  }

}
