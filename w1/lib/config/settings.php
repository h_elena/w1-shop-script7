<?php
/**
 * Массив, описывающий настройки плагина в формате
 */

return array(
  'generate'    => array(
    'value'        => 'yes',
    'title'        => 'Создать набор иконок для вывода на странице оформления заказа',
    'description'  => '',
    'control_type' => waHtmlControl::CHECKBOX,
  ),
  'merchantId'    => array(
    'value'        => '',
    'title'        => 'Индентификатор (номер кассы)',
    'description'  => 'Индентификатор (номер кассы) интернет-магазина, сгенерированный на сайте WalletOne',
    'control_type' => waHtmlControl::INPUT,
  ),
  'signatureMethod'    => array(
    'value'        => 'md5',
    'title'        => 'Метод формирования ЭЦП',
    'description'  => '',
    'control_type' => waHtmlControl::SELECT,
    'options'      => array('md5' => 'MD5', 'sha1' => 'SHA1'),    
  ),
  'signature'    => array(
    'value'        => '',
    'title'        => 'Ключ (ЭЦП) интернет-магазина',
    'description'  => 'Код, который сгенерирован в личном кабинете кассы',
    'control_type' => waHtmlControl::INPUT,
  ),
  'currencyId'    => array(
    'value'        => 643,
    'title'        => 'Идентификатор валюты по умолчанию',
    'description'  => '',
    'control_type' => waHtmlControl::SELECT,
    'options'      => array(
      643 => 'Российские рубли',
      710 => 'Южно-Африканские ранды',
      840 => 'Американские доллары',
      978 => 'Евро',
      980 => 'Украинские гривны',
      398 => 'Казахстанские тенге',
      974 => 'Белорусские рубли',
      972 => 'Таджикские сомони',
      985 => 'Злотый',
      981 => 'Лари'
    ),    
  ),
  'url'  => array(
    'value'        => '1',
    'title'        => 'Адрес страницы об оплате',
    'description'  => 'Указываем полный путь к странице с результатом оплаты http://ваш_сайт/payments.php/w1/ или https://ваш_сайт/payments.php/w1/',
    'control_type' => waHtmlControl::CHECKBOX,
  ),
  'ptenabled'  => array(
    'value'        => '',
    'title'        => 'Разрешенные платежные системы',
    'description'  => '',
    'control_type' => 'PaymentsMethod',
  ),
  'ptdisabled'  => array(
    'value'        => '',
    'title'        => 'Запрещенные платежные системы',
    'description'  => '',
    'control_type' => 'PaymentsMethod',
  ),
  'data_update'    => array(
    'value'        => time() - date('Z'),
    'title'        => '',
    'description'  => '',
    'control_type' => waHtmlControl::HIDDEN,
  ),
);
