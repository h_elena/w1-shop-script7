<?php
/**
 * Payment plugin general description
 */

return array(
    'name'        => 'Wallet One Единая касса',
    'description' => 'Wallet One Единая касса - платежный агрегатор',
    'icon'        => 'img/w1_16.png',
    'logo'        => 'img/W1_1.png',
    'vendor'      => 'webasyst',
    'version'     => '1.0.0',
    'type'        => waPayment::TYPE_ONLINE,
);
