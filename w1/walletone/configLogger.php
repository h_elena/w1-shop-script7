<?php

return [
  'rootLogger' => [
    'appenders' => ['default'],
  ],
  'appenders' => [
    'default' => [
      'class' => 'LoggerAppenderFile',
      'layout' => [
        'class' => 'LoggerLayoutPattern',
        'conversionPattern' => '%date [%logger] %message%newline'
      ],
      'params' => [
        'file' => 'wa-log/payment/w1Payment.log',
        'append' => true
      ]
    ]
  ]
];

