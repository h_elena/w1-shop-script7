<?php

namespace WalletOne\Classes;

class W1Factory{
  
  public static function createHtmlClass($config, $className, $lang = 'ru'){
    $object = null;
    switch ($className) {
      case 'diafan':
        $object = new \WalletOne\Classes\ExtraMethods\W1HtmlDiafan($config, $lang);
        break;
      case 'modx':
        $object = new \WalletOne\Classes\ExtraMethods\W1HtmlModx($config, $lang);
        break;
      case 'presta':
        $object = new \WalletOne\Classes\ExtraMethods\W1HtmlPresta($config, $lang);
        break;
      case 'readyScript':
        $object = new \WalletOne\Classes\ExtraMethods\W1HtmlReadyScript($config, $lang);
        break;
      case 'amiro':
        $object = new \WalletOne\Classes\ExtraMethods\W1HtmlAmiro($config, $lang);
        break;
      case 'drupal7commerce':
        $object = new \WalletOne\Classes\ExtraMethods\W1HtmlDrupal7Commerce($config, $lang);
        break;
      case 'shopscript':
        $object = new \WalletOne\Classes\ExtraMethods\W1HtmlShopScript($config, $lang);
        break;
      default:
        $object = new \WalletOne\Classes\StandardsMethods\W1Html($config, $lang);
        break;
    }
    
    return $object;
  }
  
  public static function createPaymentsClass($config, $className){
    $object = null;
    switch ($className) {
      case 'diafan':
        $object = new \WalletOne\Classes\ExtraMethods\W1PaymentsDiafan($config);
        break;
      case 'readyScript':
        $object = new \WalletOne\Classes\ExtraMethods\W1PaymentsReadyScript($config);
        break;
      case 'netcat':
        $object = new \WalletOne\Classes\ExtraMethods\W1PaymentsNetcat($config);
        break;
      case 'umi':
        $object = new \WalletOne\Classes\ExtraMethods\W1PaymentsUmi($config);
        break;
      case 'drupal7commerce':
        $object = new \WalletOne\Classes\ExtraMethods\W1PaymentsDrupal7Commerce($config);
        break;
      case 'shopscript':
        $object = new \WalletOne\Classes\ExtraMethods\W1PaymentsShopScript($config);
        break;
      default:
        $object = new \WalletOne\Classes\StandardsMethods\W1Payments($config);
        break;
    }
    
    return $object;
  }
  
  public static function createSettingsClass($config, $className){
    $object = null;
    switch ($className) {
      case 'modx':
        $object = new \WalletOne\Classes\ExtraMethods\W1SettingsModx($config);
        break;
      default:
        $object = new \WalletOne\Classes\StandardsMethods\W1Settings($config);
        break;
    }
    
    return $object;
  }
}

